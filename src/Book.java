import java.util.Random;
import java.util.ArrayList;
import java.util.Collections;

public class Book implements Actions {

    Random rand = new Random();
    ArrayList<Integer> values = new ArrayList<>();

    //    Attributes
    private String titulo;
    private String autor;
    private int totPaginas;
    private boolean aberto;
    private Person leitor;
    private int paginaAtual;

//    Methods

    //    Constructor
    Book(String titulo, String autor, int totPaginas, Person leitor) {
        setTitulo(titulo);
        setAutor(autor);
        setTotPaginas(totPaginas);
        setAberto(false);
        setLeitor(leitor);
        setPaginaAtual(0);
    }

    //    Getters and Setters
    private String getTitulo() {
        return this.titulo;
    }

    private void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getTotPaginas() {
        return totPaginas;
    }

    public void setTotPaginas(int totPaginas) {
        this.totPaginas = totPaginas;
    }

    public boolean isAberto() {
        return aberto;
    }

    public void setAberto(boolean aberto) {
        this.aberto = aberto;
    }

    public Person getLeitor() {
        return leitor;
    }

    public void setLeitor(Person leitor) {
        this.leitor = leitor;
    }

    public int getPaginaAtual() {
        return paginaAtual;
    }

    public void setPaginaAtual(int paginaAtual) {
        this.paginaAtual = paginaAtual;
    }

    @Override
    public void abrir() {
        setAberto(true);
    }

    @Override
    public void fechar() {
        setAberto(false);
    }

    @Override
    public void folhear() {
        int vezes = rand.nextInt(10) + 1;
        int contador = 1;
        int aux;

        System.out.println("\nFolheando....");

        while ((contador < getTotPaginas() / 2) && (contador <= vezes)) {
            aux = rand.nextInt(this.getTotPaginas() + 1);
            while (values.contains(aux))
                aux = rand.nextInt(this.getTotPaginas() + 1);
            values.add(aux);
            contador++;
        }

        Collections.sort(values);
        System.out.print("...");
        for (int i : values)
            System.out.print(i + "...");
        System.out.println();
    }

    @Override
    public void avancarPag() {
        setPaginaAtual(getPaginaAtual() + 1);
    }

    @Override
    public void voltarPag() {
        setPaginaAtual(getPaginaAtual() - 1);
    }

    @Override
    public void mudarLeitor(Person leitor) {
        setLeitor(leitor);
    }

    @Override
    public void statusDoLivro() {
        String state = (this.isAberto()) ? "aberto" : "fechado";
        System.out.print("O livro \"" + this.getTitulo() + "\" está " + state + ", possui " + this.getTotPaginas() + " páginas, ");
        if (isAberto())
            System.out.print("está aberto na página " + this.getPaginaAtual() + " ");
        System.out.print("e está sendo lido por " + this.leitor.getName() + " que tem " + this.leitor.getAge() + " anos e é do sexo ");
        if (Character.toUpperCase(this.leitor.getSex()) == 'M')
            System.out.println("masculino.");
        else if (Character.toUpperCase(this.leitor.getSex()) == 'F')
            System.out.println("feminino.");
        else
            System.out.println("indefinido.");
    }

}
