public class BookExercise {
    public static void main(String[] args) {

        Person pessoa = new Person("Guilherme", 17, 'M');
        Person leitor = new Person("Beatriz", 16, 'F');
        Person l = new Person("Juliana", 17, 'T');
        Book livroUm = new Book("O programador pragmático", "Desconhecido", 792, pessoa);
        Book livroDois = new Book("C++ Orientado a Objetos", "David Sena", 467, leitor);

        livroUm.statusDoLivro();
        livroUm.abrir();
        livroUm.folhear();
        livroUm.avancarPag();
        livroUm.avancarPag();
        livroUm.avancarPag();
        livroUm.avancarPag();
        livroUm.avancarPag();
        livroUm.avancarPag();
        livroUm.statusDoLivro();
        livroDois.statusDoLivro();
        livroDois.folhear();

        livroDois.statusDoLivro();
        livroDois.mudarLeitor(l);
        livroDois.statusDoLivro();

    }
}
