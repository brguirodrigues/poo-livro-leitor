public class Person {

    //    Attributes
    private String name;
    private int age;
    private char sex;

//    Methods

    //    Constructor
    public Person(String name, int age, char sex) {
        setName(name);
        setAge(age);
        setSex(Character.toUpperCase(sex));
    }

    //    Getters and Setters
    public String getName() {
        return this.name;

    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }


}
